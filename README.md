This is just a collection of some initial states for my personal use.

I'm currently using these in a masterless configuration.

## bootstrap

I have successfully bootstrapped these states on a small number of nodes in the
following manner. Note that this is insecure and should not be trusted. It is
provided here as an example and a starting point only.

Install the latest stable version of a minion using an insecure one-liner.
The `-X` option should prevent a minion service from starting up.
```bash
curl -s https://bootstrap.saltstack.com | sh -s -- -X
```

If you end up with a running minion service, stop it.
```bash
systemctl disable salt-minion
systemctl stop salt-minion
```

Configure the node to be masterless
```bash
cat << EOF > /etc/salt/minion.d/10-masterless.conf
file_client: local
EOF
```

Clone this repo some place.
```bash
git clone https://gitlab.com/cdarwin/salt.git
```

Bind mount the states tree to the expected location
```bash
mkdir -p /srv/salt
mount --bind salt/states /srv/salt
```

Apply the `core` collection of states to setup and configure gitfs and some
utilities.
```bash
salt-call state.apply core
```

Once gitfs is set up, you should be able to remove this repo from disk
```bash
umount /srv/salt
rm -rf salt
```

## cloud-config

I am supremely lazy, so I have added a `cloud-init` `cloud-config` that I have
used as user-data for provisioning Debian Jessie nodes in DigitalOcean.

Using this cloud-config file, I can quickly provision a new instance in
DigitalOcean with something like the following:

```bash
$ doctl compute droplet create deb01 \
    --image debian-8-x64 \
    --region sfo1 \
    --size 512mb \
    --enable-private-networking \
    --ssh-keys 1234567 \
    --user-data-file cloud-init \
    --wait
```
