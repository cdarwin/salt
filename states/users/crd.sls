include:
  - sudo
  - ssh

crd:
  user.present:
    - groups:
      - sudo
    - shell: /bin/bash
    - optional_groups:
      - docker
    - require:
      - pkg: sudo
  ssh_auth.present:
    - user: crd
    - source: salt://users/files/crd_keys
  file.managed:
    - name: /etc/sudoers.d/crd
    - contents:
      - 'crd ALL=(ALL:ALL) NOPASSWD: ALL'

extend:
  disable remote superuser login:
    file:
      - require:
        - user: crd
  sudo:
    service:
      - watch:
        - file: /etc/sudoers.d/crd
