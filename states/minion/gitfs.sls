gitfs package dependencies:
  pkg.installed:
    - pkgs:
      - python-pip
      - git

gitfs provider library:
  pip.installed:
    - name: GitPython
    - reload_module: True

gitfs minion config:
  file.managed:
    - name: /etc/salt/minion.d/30-gitfs.conf
    - source: salt://minion/files/30-gitfs.conf
