include:
  - .gitfs

quiet down state output:
  file.managed:
    - name: /etc/salt/minion.d/99-output.conf
    - contents: |
        states_verbose: False
        state_output: changed
