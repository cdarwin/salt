openssh-server:
  pkg.installed

ssh:
  service.running:
    - watch:
      - file: /etc/ssh/sshd_config

disable remote superuser login:
  file.replace:
    - name: /etc/ssh/sshd_config
    - pattern: ^(PermitRootLogin).*
    - repl: \g<1> no
    - append_if_not_found: True
    - not_found_content: PermitRootLogin no

disable tunnelled clear text passwords:
  file.replace:
    - name: /etc/ssh/sshd_config
    - pattern: ^(PasswordAuthentication).*
    - repl: '\g<1> no'
    - append_if_not_found: True
    - not_found_content: PasswordAuthentication no
